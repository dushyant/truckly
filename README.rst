Truckly
=======

A simple API that gives a list of food trucks in San Fransisco. 

swagger: http://truckly.api.dush.me/swagger/

api: http://truckly.api.dush.me/trucks/

client: http://truckly.dush.me/


Design
------

The data source of the food trucks are the `SF Open Data
<https://data.sfgov.org/Economy-and-Community/Mobile-Food-Facility-Permit/rqzj-sfat]>`_
guys.

This application syncs with the source and saves all the relevant data in its
own database. There are a couple reasons for this:

1. The original source has a lot of information that we don't need.
2. Unifiying the API contract, if we add another data source

The database used in here is Postgres@9.4 with Postgis extension for geo
support.

This is a simple Flask application, using SqlAlchemy.


Usage
-----

There is swagger doc present at http://truckly.api.dush.me/swagger/

For basic usage

Getting all the trucks, ``GET /trucks/``::

    $ curl -X GET http://truckly.api.dush.me/trucks/'

Fitlering a truck depending on status, ``GET /vars/?status=APPROVED``::

    $ curl -X GET http://truckly.api.dush.me/trucks/?status=APPROVED

Getting trucks with a certain radius, ``GET /trucks/?latitude=<float>&longitude=<float>&distance=<integer>``::

    $ curl -X GET http://truckly.api.dush.me/trucks/?latitude=37.7917&longitude=-122.3975&distance=1000


TODO
----

1. Make the sync idempotent, to allow to get latest data (maybe a nightly job)
2. Tags - discover unique tags using the description and allow filters
3. Test cases for within radius filtering
