from geoalchemy2.shape import to_shape, from_shape
from marshmallow.fields import Field
from shapely.geometry import mapping, shape, Point as ShapelyPoint

DEFAULT_SRID = 4326


class Geometry(Field):
    default_error_messages = {
        'invalid': 'Not a valid geometry type.'
    }

    def _serialize(self, value, attr, obj):
        if value is None:
            return None
        el = to_shape(value)
        self._check_geometry_type(el)
        return mapping(el)

    def _deserialize(self, value, attr, data):
        try:
            el = shape(value)
        except Exception:
            self.fail('invalid')
        self._check_geometry_type(el)
        return from_shape(el, DEFAULT_SRID)

    def _check_geometry_type(self, el):
        pass


class Point(Geometry):
    default_error_messages = {
        'invalid': 'Not a valid point.'
    }

    def _check_geometry_type(self, el):
        if not isinstance(el, ShapelyPoint):
            self.fail('invalid')
