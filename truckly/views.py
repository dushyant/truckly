from flask import jsonify, request
from sqlalchemy import func

from truckly import app, db
from truckly.decorators import crossdomain
from truckly.models import Truck
from truckly.schemas import TruckSchema
from truckly.utils import FilterParser, point_from_coordinate


class TruckFilterParser(FilterParser):
    model = Truck
    allowed_filters = ['status', 'facility_type']

    def _handle_location(self, query, args):

        try:
            longitude = float(args['longitude'])
            latitude = float(args['latitude'])
        except KeyError, ValueError:
            pass

        else:
            distance = args.get('distance', 100)  # default 100 m
            point = point_from_coordinate(longitude, latitude)
            query = query.filter(func.ST_DWithin(self.model.location, point, distance))

        return query

    def filter_query(self, query, args):
        query = super(TruckFilterParser, self).filter_query(query, args)
        query = self._handle_location(query, args)
        return query


@app.route('/trucks/', methods=['GET'])
@crossdomain(origin='*')
def trucks():
    trucks = db.session.query(Truck).filter(Truck.location != None)
    trucks = TruckFilterParser().filter_query(trucks, request.args)
    truck_schema = TruckSchema(many=True)
    result = truck_schema.dump(trucks.all())
    return jsonify({'data': result.data})
