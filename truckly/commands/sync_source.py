import requests

from truckly import db
from truckly.models import Truck
from truckly.utils import point_from_coordinate


SOURCE_URL = 'https://data.sfgov.org/resource/rqzj-sfat.json'


SOURCE_MAPPER = {
    'applicant': 'name',
    'objectid': 'ext_id',
    'fooditems': 'description',
    'facilitytype': 'facility_type',
    'address': 'address',
    'dayshours': 'opening_times',
    'status': 'status',
}


def transform_truck(truck):
    transformed_truck = {}
    for key, value in SOURCE_MAPPER.items():
        transformed_truck[value] = truck.get(key)

    try:
        location = point_from_coordinate(
            float(truck.get('location')['longitude']),
            float(truck.get('location')['latitude'])
        )
    except (KeyError, TypeError):
        location = None
    else:
        transformed_truck.update({'location': location})

    return Truck(**transformed_truck)


def sync_data():
    trucks = requests.get(SOURCE_URL)

    for truck in trucks.json():
        truck = transform_truck(truck)
        print truck
        db.session.add(truck)

    db.session.commit()


if __name__ == '__main__':
    sync_data()
