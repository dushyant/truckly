from truckly import app
from truckly.views import *  # noqa

if __name__ == "__main__":
    app.run()
