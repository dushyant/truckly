from flask import json
from hamcrest import assert_that, has_items, has_entries, instance_of, is_
from six import text_type

from truckly.models import Truck
from truckly.tests import TrucklyTests
from truckly.utils import point_from_coordinate


class TestEmptyTrucks(TrucklyTests):

    def test_get_trucks(self):
        resp = self.app.get('/trucks/')
        trucks = json.loads(resp.data)
        self.assertEqual(trucks, {'data': []})


class TestGetTrucks(TrucklyTests):

    fixtures = [
        Truck(
            name='First Truck',
            facility_type='Truck',
            ext_id='10',
            status='APPROVED',
            location=point_from_coordinate(-122.397, 37.791)
        ),
        Truck(
            name='Second Truck',
            facility_type='Truck',
            ext_id='12',
            status='REJECTED',
            location=point_from_coordinate(-122.397, 37.791)
        ),
        Truck(
            name='Push cart being thrown around',
            facility_type='Push Cart',
            ext_id='13',
            status='APPROVED',
            location=point_from_coordinate(-122.397, 37.791)
        )
    ]

    def test_get_all_trucks(self):
        resp = self.app.get('/trucks/')
        trucks = json.loads(resp.data)
        assert_that(trucks, has_entries({
            'data': has_items(has_entries({
                'name': instance_of(text_type),
                'id': instance_of(int),
                'status': instance_of(text_type),
                'facility_type': instance_of(text_type),
                'location': has_entries({
                    'type': u"Point",
                    'coordinates': has_items(instance_of(float), instance_of(float))
                }),
            }))
        }))

    def test_get_trucks_filter_approved(self):
        resp = self.app.get('/trucks/?status=APPROVED')
        trucks = json.loads(resp.data)
        assert_that(trucks, has_entries({
            'data': has_items(has_entries({
                'name': instance_of(text_type),
                'id': instance_of(int),
                'status': is_(u"APPROVED"),
                'facility_type': instance_of(text_type),
                'location': has_entries({
                    'type': u"Point",
                    'coordinates': has_items(instance_of(float), instance_of(float))
                }),
            }))
        }))

    def test_get_trucks_filter_rejected_trucks(self):
        resp = self.app.get('/trucks/?status=APPROVED&facility_type=Truck')
        trucks = json.loads(resp.data)
        assert_that(trucks, has_entries({
            'data': has_items(has_entries({
                'name': instance_of(text_type),
                'id': instance_of(int),
                'status': is_(u"APPROVED"),
                'facility_type': is_(u"Truck"),
                'location': has_entries({
                    'type': u"Point",
                    'coordinates': has_items(instance_of(float), instance_of(float))
                }),
            }))
        }))
