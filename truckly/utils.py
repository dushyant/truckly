from geoalchemy2.shape import from_shape
from shapely.geometry import shape


class FilterParser(object):
    """ Utility to create a SqlAlchemy query from a dictionary of arguements.

    Subclass this class and specify the model and the filters that you would
    want to allowed on that class. by calling the `filter_query` method of this
    class you would get query that has all the filters applied.

    Eg:
        UserFilterParser(FilterParser):
            model = User
            allowed_filters = ['gender', 'age']

       query = sqlalchemy.orm.query.Query()

       user_filter_parser = UserFilterParser()
       query = user_filter_parser.filter_query(query, {'gender': 'Male'})

    This is especially useful to transalte request query paramaters
    (?gender=Male) to SqlAlcheymQuery.
    """
    model = None
    allowed_filters = []

    def filter_query(self, query, args):
        for key in self.allowed_filters:
            try:
                value = args[key]
            except KeyError:
                continue

            column = getattr(self.model, key, None)
            if value == 'null':
                value = None
            filt = getattr(column, '__eq__')(value)
            query = query.filter(filt)

        return query


def point_from_coordinate(longitude, latitude):
    shape_dict = {
        'type': 'Point',
        'coordinates': [longitude, latitude]
    }
    shape_point = shape(shape_dict)
    return from_shape(shape_point, srid=4326)
