from marshmallow import fields, Schema

from truckly.fields import Point


class TruckSchema(Schema):

    id = fields.Integer()
    name = fields.String()
    description = fields.String()
    status = fields.String()
    facility_type = fields.String()
    address = fields.String()
    opening_times = fields.String()
    location = Point()
