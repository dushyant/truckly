from geoalchemy2 import Geography

from truckly import db


class Truck(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ext_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String)
    facility_type = db.Column(db.String)
    description = db.Column(db.String)
    address = db.Column(db.String)
    opening_times = db.Column(db.String)
    status = db.Column(db.String)
    location = db.Column(Geography(geometry_type='POINT', srid=4326))

    def __repr__(self):
        return '<Truck %r>' % self.name
